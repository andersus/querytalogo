# O QUE #
Este projeto tem por objetivo unificar e tornar de forma dinâmica a consulta de Querys utilizadas pelo Quality.


# PORQUE #

Há uma perda substancial no tempo que executamos nossos relatórios, por não termos uma busca adequada no diretório, com palavras-chaves
ou descrição do que executa determinada query.

# ONDE SERÁ FEITO? #

* Departamento: Quality
* Local: Centro de Comando e Operações de ti
* Empresa: Globalweb Outsourcing

# QUANDO SERÁ FEITO? #

* Fevereiro/2018 à Março/2018
 - Ver Cronograma de Fases

# POR QUEM SERÁ FEITO? #

* Anderson Araujo da Silva 
* Paulo Guilherme De Castro Chicherchio

# COMO SERÁ FEITO? #

* Back-end: Python
* Front-end: Html5, CSS, Bootstrap